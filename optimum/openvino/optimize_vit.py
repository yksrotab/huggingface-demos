# Load original model
# Vision Transformer model fine-tuned on the food-101 dataset

from transformers import AutoFeatureExtractor, AutoModelForImageClassification, pipeline

model_id = "juliensimon/autotrain-food101-1471154050"
task_type = "image-classification"

save_dir = "./quantized_model"
model = AutoModelForImageClassification.from_pretrained(model_id)
feature_extractor = AutoFeatureExtractor.from_pretrained(model_id)
pipe = pipeline(task_type, model=model, feature_extractor=feature_extractor)

# Evaluate original model

import evaluate
from datasets import load_dataset

dataset_id = "food101"
data = load_dataset(dataset_id, split="validation[:10%]")
print(data)
metric = evaluate.load("accuracy")
evaluator = evaluate.evaluator(task_type)


def evaluate_pipeline(pipeline):
    results = evaluator.compute(
        model_or_pipeline=pipeline,
        data=data,
        metric=metric,
        label_column="label",
        label_mapping=model.config.label2id,
    )
    return results


results = evaluate_pipeline(pipe)
print(results)

# Create a quantizer object

from optimum.intel.openvino import OVConfig, OVQuantizer

quantization_config = OVConfig()
print(quantization_config)

quantizer = OVQuantizer.from_pretrained(model)
calibration_dataset = quantizer.get_calibration_dataset(
    dataset_id,
    num_samples=1000,
    dataset_split="train",
)

# Preprocess the calibration dataset

import torch
from torchvision.transforms import CenterCrop, Compose, Normalize, Resize, ToTensor

# Preprocess the calibration dataset


normalize = Normalize(mean=feature_extractor.image_mean, std=feature_extractor.image_std)
_val_transforms = Compose(
    [
        Resize(feature_extractor.size),
        CenterCrop(feature_extractor.size),
        ToTensor(),
        normalize,
    ]
)


def val_transforms(example_batch):
    example_batch["pixel_values"] = [
        _val_transforms(pil_img.convert("RGB")) for pil_img in example_batch["image"]
    ]
    return example_batch


calibration_dataset.set_transform(val_transforms)

# Define a collator function to prepare input features


def collate_fn(examples):
    pixel_values = torch.stack([example["pixel_values"] for example in examples])
    labels = torch.tensor([example["label"] for example in examples])
    return {"pixel_values": pixel_values, "labels": labels}


# Apply static quantization and export the quantized model to OpenVINO IR format

quantizer.quantize(
    quantization_config=quantization_config,
    calibration_dataset=calibration_dataset,
    data_collator=collate_fn,
    remove_unused_columns=False,
    save_directory=save_dir,
)

feature_extractor.save_pretrained(save_dir)

# Evaluate the quantized model

from optimum.intel.openvino import OVModelForImageClassification
from transformers import pipeline

ov_model = OVModelForImageClassification.from_pretrained(save_dir)
pipe = pipeline(task_type, model=ov_model, feature_extractor=feature_extractor)
results = evaluate_pipeline(pipe)
print(results)
